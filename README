Deduce 1.3.0
------------

1. Introduction

Welcome to Deduce.  Deduce is a deductive reasoning AI that learns
information from you by accepting English sentences.  Once you've
taught it something, you can ask it questions in the form of yes/no
questions.  Deduce will search its database, making any logical
connections it can, and return an answer of yes, no, or I don't know.
An example should make this a little clearer.

> Spot is a dog.
> A dog is an animal
> Is Spot an animal?
YES, SPOT IS AN ANIMAL

After asking a question, you can ask Deduce why it answered the way it
did by asking it "why".  In this example, you would get back the
following line of reasoning:

BECAUSE:
  SPOT IS A DOG
  A DOG IS AN ANIMAL

This is a very simple example, but the logic can become much more
complex as Deduce learns more.

2. Negative Logic

Deduce is able to handle statements that express negative logic.  This
is the case when you say one thing is not something else.  To continue
in the previous example, we could add the following statements:

> A dog is not a cat
> Is Spot a cat
NO, SPOT IS NOT A CAT

> why
BECAUSE:
  SPOT IS A DOG
  A DOG IS NOT A CAT

3. All or none principle

When you supply a statement to Deduce, it assumes that the statement
is always true, or in the case of negative logic, never true.  So if
you say "A bird can fly", it understands that as saying "every bird
can fly" and "a bird can always fly".  If you later say "a penguin is
a bird" and "a penguin can not fly", Deduce will refuse to accept it,
since it seems to go against previous logic.

This type of thing can sometimes give surprising results.  Consider
the following statements:

> A square is a square
> A rectangle is not a square
> Is a square a rectangle?
NO, A SQUARE IS NOT A RECTANGLE

The problem here lies in the statement, "A rectangle is not a square".
While this may seem like a true statement, it isn't always true.  Some
rectangles are squares, but Deduce makes the assumption that this is
never the case, and therefore thinks the opposite must also be false
(if a rectangle is never a square, then a square can never be a
rectangle).

4. Positive versus negative

In the previous example, we saw that if foo is not bar, then bar
cannot be foo.  If the logic is positive (foo is bar), then the
opposite cannot be assumed (we can't automatically say that bar is
foo).  For example, we might say that all hatchbacks are cars, but
that doesn't mean all cars are hatchbacks.

Sometimes we can make discoveries based on differing attributes of two
subjects.  Take the following example:

> A bird can fly
> A dog can not fly

From this, we know that a bird is not a dog, and a dog is not a bird.
Furthermore, anything that is found to be able to fly can't be a dog,
and anything which can't fly can't be a bird.

5. Limitations

There are a few things that Deduce doesn't handle well as of this
writing.

* As mentioned previously, Deduce follows an all or none principle.
It assumes facts are either always true or always false.  As you've
seen this isn't always the case in natural language.

* Deduce doesn't understand that words like dog and dogs are different
forms of the same word.  It also doesn't see the link between
different forms of the same verb.  The one exception to this is forms
of the verb "to be".

* Deduce doesn't understand tense (past, present, future).  So it
sees no difference between "the water has evaporated" and "the water
will evaporate".

6. Interfacing with Deduce

Deduce will accept sentences in one of two forms:

   subject verb
   subject verb object

The subject and object are nouns which can be prefixed with an article
(a, an, the).  The verb can be a single word, or combined with a
helping verb.  Sentences can also contain a word that makes it a
negative (no, not, never).

When you enter a sentence, if Deduce is happy with it, it will respond
with "OK".  If it has a problem, it will return an error.  "THAT CAN'T
BE RIGHT" means that you are entering something which Deduce knows to
be false.  "YEAH, I KNOW" means Deduce already knows what you're
trying to tell it.

The sentence can also be worded as a question:

   verb subject
   verb subject object

In this case, you are asking something from Deduce.  Questions need to
be yes/no type questions for Deduce to understand them and to be able
to answer them.  Deduce will answer with YES, NO, or I DON'T KNOW.

There are also a number of commands which can be entered on the
command line.  They are detailed below.

6.1 Commands
6.1.1 Load <filename>

Use this command to load a file from disk.  Files are simple text
files which contain a single fact on each line.

6.1.2 Save <filename>

Use this command to save your session to a file.  The saved file can
be reloaded at a later time with the load command.

6.1.3 Quit

When you're tired of interfacing with Deduce, this command will end
your session.  Make sure to issue a save command first if you wish to
keep your session.

6.1.4 Why

After asking a question, this command can be used to have Deduce
explain its response.  If the response was "I DON'T KNOW", it won't be
able to provide you with anything.  This command can also be used if
you try to enter a fact and Deduce refuses it.

6.1.5 Divulge

This will output a list of everything that's been entered.  It's the
same data that gets written to disk when you issue a save command,
but it gets displayed to the screen instead.

6.1.6 Forget

This causes Deduce to forget everything it's been taught.  This can be
used before loading a new set of data, or just to start over with a
clean slate.

6.1.7 Help

This command displays a brief help screen.

7. Thanks to

Many thanks to the following people:

 The authors of Mind.rexx (util/rexx/Mindrx.lha on Aminet). It was what
 inspired me to write Deduce.

 Ronald Carnell, author of "Smart Alec" (Compute! Sept. 1987).  He supplied
 the basic algorithm used in "Smart Alec", which I took and expanded upon
 to create Deduce.

 Eric Augustine for putting together the help systems in earlier versions
 of Deduce.

 Hendrik Fuss, Arthur Hagen, Stefan Dube, Simon Stelling, and anybody else
 I've forgotten for the suggestions, bug fixes, and overall support
 through the years.  Believe me, it's been very much appreciated.