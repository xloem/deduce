#!/usr/bin/env python2

import random
import sys
from deduce import *

def ui():
    b = brain()
    print "HI, I'M DEDUCE.  FILL MY HEAD WITH TRIVIA, THEN QUIZ ME ON IT."
    print "I'M A GOOD LISTENER.  REALLY I AM."
    print "TYPE 'HELP' FOR SYNTAX HELP."

    while (1):
        sys.stdout.write("> ")
        quote = sys.stdin.readline()

        if (quote == ''):
            # CTRL-D was entered
            quote = "QUIT"

        try:
            # Look for a command in the input string
            quote = quote.strip()
            cmd = quote.split()

            if (len(cmd) == 0):
                facts = b.divulge()
                if (len(facts)):
                    print random.choice(b.divulge())
                else:
                    print "DEDUCE IS DEDUCE"
                continue

            cmd[0] = cmd[0].upper()

            if (cmd[0] == "HELP"):
                print "LOAD <filename> - Load a session from a file"
                print "SAVE <filename> - Save your current session to a file"
                print "FORGET - Clear Deduce's memory"
                print "WHY - Have Deduce explain its answer to a question"
                print "DESCRIBE - Have Deduce describe a subject"
                print "DIVULGE - Dump Deduce's memory to the screen"
                print "DIVULGE_DETAIL - Like DIVULGE but shows parts of speech"
                print "HELP - Display this help text"
                print "ABOUT - Display information about Deduce"
                print "QUIT - Exit the program"
                print "<" + ",".join(sentence.verbs + sentence.helping_verbs) + "> - Query Deduce's memory"
                print "<anything else> - Try to teach a fact to Deduce"

            elif (cmd[0] == "ABOUT"):
                print "Deduce 1.4.0, Copyright (C) 1995-2004  James Williams"
                print "This program is licensed under the GNU General Public"
                print "Licence (GPL) and may be distributed freely.  This"
                print "program comes with ABSOLUTELY NO WARRANTY.  For details,"
                print "please refer to the file COPYING, which should have"
                print "been included with this program."
            
            elif (cmd[0] == "LOAD"):
                b.load(cmd[1])
                print "SESSION LOADED"
                
            elif (cmd[0] == "SAVE"):
                b.save(cmd[1])
                print "SESSION SAVED"
                
            elif (cmd[0] == "WHY"):
                question = join(cmd[1:], " ")
                reason = b.why(question)

                if (len(reason) == 0):
                    print "*SHRUG*"
                else:
                    print "BECAUSE:"
                    for r in b.why(question):
                        print "  "+r.__str__()

            elif (cmd[0] == "DESCRIBE"):
                subject = join(cmd[1:], " ")
                description = b.describe_subj(subject)
                if len(description) < 2:
                    print "DESCRIBE WHAT NOW?"
                else:
                    print "{}:".format(subject)
                    for line in description:
                        print line
             
            elif (cmd[0] == "DIVULGE"):
                for f in b.divulge():
                    print f

            elif (cmd[0] == "DIVULGE_DETAIL"):
                for f in b.divulge():
                    parts = f.pos_tuples()
                    print " ".join(("{}:{}".format(pos, word) for pos, word in parts))

            elif (cmd[0] == "FORGET"):
                b = brain()
                print "FORGOTTEN"

            elif (cmd[0] == "QUIT"
                  or cmd[0] == "EXIT"
                  or cmd[0] == "BYE"
                  or cmd[0] == "STOP"):
                print "GOODBYE"
                return

            else:
                s = sentence(quote)
                if (s.is_question()):
                    [ a, ans, reas ] = b.query(quote)
                    print ans
                else:
                    response = b.learn(quote)
                    print response["MSG"]

        except (DeduceError, IOError), err:
            print "**", err

if __name__ == "__main__":                
    ui()
