#!/usr/bin/env python2
#
# $Id$
# Copyright (C) 1995-2004  James Williams
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Deduce is a deductive reasoning AI program.  Enter simple statements
# at the prompt, then ask yes/no questions on what you just entered.
# For example:
#   > Spot is a dog
#   > A dog is an animal
#   > Is Spot an animal?
#   YES
#
# $Log$
# Revision 1.4  2004/01/28 19:03:53  virtex
# Made the load and save commands less fragile.  Also fixed a couple
# spelling errors.
#
# Revision 1.3  2004/01/25 21:22:16  virtex
# Added the ability to convert between first and second person.
#
# Revision 1.2  2004/01/25 02:41:26  virtex
# Added commands FORGET, ABOUT, and HELP.  Added the ability to handle
# contractions.  Also made a few cosmetic changes.
#
# Revision 1.1.1.1  2004/01/22 02:52:19  virtex
# Initial import
#
#

import sys
import copy
from string import *

###########################################################################
##
## This class is Deduce's error handling exception.  Many of the
## methods contained within this module will raise this exception
## when something fails.
##
###########################################################################
class DeduceError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

###########################################################################
##
## A fact sentence is a collection of words and phrases.  Deduce
## parses input by first turning it into a sentence, then translating
## that into a fact.
##
###########################################################################
class sentence:
    adjectives = [ "A", "AN", "THE", "ALL", "EVERY", "NONE", "MY", "YOUR" ]
    negatives = [ "NO", "NONE", "NOT", "CANNOT", "CANT", "WONT",
                  "DOESNT", "DONT", "ISNT", "ARENT", "AINT" ]
    verbs = [ "IS", "ARE", "AM", "BE", "WAS", "WERE", "BE" ]
    helping_verbs = [ "DO", "DOES", "CAN", "WILL", "HAS", "HAVE" ]
    contractions = { "DONT":   ["DO", "NOT"],
                     "DOESNT": ["DOES", "NOT"],
                     "ISNT":   ["IS", "NOT"],
                     "ARENT":  ["ARE", "NOT"],
                     "WONT":   ["WILL", "NOT"],
                     "CANT":   ["CAN", "NOT"],
                     "CANNOT": ["CAN", "NOT"],
                     "WASNT":  ["WAS", "NOT"],
                     "WERENT": ["WERE", "NOT"],
                     "IM":     ["I", "AM"],
                     "ILL":    ["I", "WILL"],
                     "YOURE":  ["YOU", "ARE"],
                     "YOULL":  ["YOU", "WILL"] }
                     
    ##
    ## Initialize a sentence object
    ##
    ## Inputs:
    ##   string: The sentence in string format
    ##
    ## Returns:
    ##   A sentence object
    ##
    def __init__(self, string):
        # Canonicalize the sentence
        string = string.strip()
        string = string.upper()
        string = string.translate(maketrans("",""), ".?!,'\n\r\t")

        # Break the sentence into individual words
        self.words = []
        
        # Expand any contractions
        for word in string.split():
            if (word in self.contractions):
                self.words += self.contractions[word]
            else:
                self.words.append(word)

        self.index = 0

        # Mark the sentence as a question, if it is
        if (self.is_question()):
            self.question = 1
        else:
            self.question = 0

        # Store the helping verb and verb
        self.negative = 0
        self.helping_verb = ""
        self.verb = ""

    ##
    ## Read successive words from the sentence.  Each call to this method
    ## will return the next word, until the end of the sentence is reached,
    ## at which time this method will return None.
    ##
    ## Inputs:
    ##   none
    ##
    ## Returns:
    ##   A string containing a word from the sentence, or None if no
    ##   words remain.
    ##
    def next_word(self):
        if self.index >= len(self.words):
            return None
        else:
            self.index += 1
            return self.words[self.index-1]

    ##
    ## Backup the stream by one word
    ##
    ## Inputs:
    ##   none
    ##
    ## Returns:
    ##   none
    ##
    def prev_word(self):
        if (self.index > 0):
            self.index -= 1

    ##
    ## Rewind the index so the next word read will be the first in the sentence
    ##
    ## Inputs:
    ##   none
    ##
    ## Returns:
    ##   none
    ##
    def rewind_words(self):
        self.index = 0

    ##
    ## Read a phrase (noun phrase, verb phrase) from the input sentence
    ##
    ## Inputs:
    ##   none
    ##
    ## Returns:
    ##   A dictionary object containing the phrase -- the primary word,
    ##   the adjective or helping verb, whether or not the phrase is
    ##   negated, any error messages, and the type of phrase (if known).
    def next_phrase(self):
        phrase = { "negative": 0, "type": "unknown", "error": None,
                   "adj": "", "helping_verb": "", "primary": "" }
        word = self.next_word()
        state = 0

        while (1):
            if (word and word in self.negatives):
                self.negative = 1
                phrase["negative"] += 1
            else:
                if (state == 0): # New phrase
                    if (word == None):
                        phrase["type"] = "eol"
                        state = 99
                    elif (word in self.adjectives):
                        phrase["adj"] = word
                        phrase["type"] = "noun phrase"
                        state = 1
                    elif (word in self.helping_verbs):
                        if (self.question == 0):
                            phrase["helping_verb"] = word
                            phrase["type"] = "verb phrase"
                            state = 1
                        else:
                            phrase["helping_verb"] = word
                            phrase["type"] = "helping verb"
                            state = 99
                    elif (word in self.verbs):
                            phrase["primary"] = word
                            phrase["type"] = "verb"
                            state = 99
                    else:
                        phrase["primary"] = word
                        state = 99

                elif (state == 1): # Expecting a primary word
                    # This word should be a noun.  Just to be sure...
                    if (word == None):
                        # Error
                        phrase["error"] = "Unexpected end of sentence"
                    if (word in self.adjectives or
                        word in self.helping_verbs):
                        # Error
                        phrase["error"] = "Unexpected word \""+word+"\""
                    else:
                        phrase["primary"] = word

                    state = 99

                elif (state == 99):
                    # End of phrase reached
                    if (word != None):
                        self.prev_word()
                        
                    return phrase

                else:
                    # We should never reach this point
                    print "*** Fatal internal error in next_phrase method"


            word = self.next_word()
            

    ##
    ## Determine if this sentence is a question
    ##
    ## Inputs:
    ##   none
    ##
    ## Returns:
    ##   0: It's not a question
    ##   1: It is a question
    ##
    def is_question(self):
        if (self.words[0] in self.verbs or self.words[0] in self.helping_verbs):
            return 1
        else:
            return 0

###########################################################################
##
## A fact is a single piece of information.  It's what is stored in
## Deduce's brain when you enter a statement at the command line.  This
## class is used to store and print individual facts.
##
###########################################################################
class fact:
    # Class members
    subj_adj     = ""
    subj         = ""
    helping_verb = ""
    verb         = ""
    orig_verb    = ""
    obj_adj      = ""
    obj          = ""
    question     = 0
    negative     = 0
    seen         = 0

    to_be = [ "IS", "ARE", "AM", "BE", "WAS", "WERE" ]

    ##
    ## Initialize a fact from a statement
    ##
    ## Inputs:
    ##   statement: A string to convert to a fact
    ##
    ## Returns:
    ##   a fact object
    ##
    def __init__(self, statement):
        if (statement == ""):
            self.error = "I BEG YOUR PARDON?"
        else:
            self.translate(statement)

    ##
    ## Provide a fact as a list of part-of-speech,word tuples
    ##
    ## Inputs:
    ##   none
    ##
    ## Returns:
    ##   A list-of-tuples representing the fact (a sentence)
    ##
    def pos_tuples(self):
        out = []

        # Figure out how to represent a negative
        if self.negative == 1:
            if self.helping_verb:
                verb_ph = self.helping_verb+" NOT "+self.verb
            else:
                verb_ph = self.orig_verb+" NOT"
        else:
            verb_ph = join([self.helping_verb, self.orig_verb], " ").strip()

        # Combine the words into a list, removing any empty words
        for pos, word in [('a', self.subj_adj),
                     ('s', self.subj),
                     ('v', verb_ph),
                     ('a', self.obj_adj),
                     ('o', self.obj)]:
            if word != "" and word[0] != ".":
                out.append((pos, word))

        # Finally, return the result
        return out

    ##
    ## Output a fact as a string
    ##
    ## Inputs:
    ##   none
    ##
    ## Returns:
    ##   A string representing the fact (a sentence)
    ##
    def __str__(self):
        # Get the parts of this sentence
        out = self.pos_tuples()

        # Extract only the words
        out = [word for pos, word in out]

        # Render the list as a sentence
        return join(out, " ")

    ##
    ## Convert a string to an internal representation and store it
    ## within the fact object.
    ##
    ## Inputs:
    ##   statement: A string to convert to a fact
    ##
    ## Returns:
    ##   none
    ##
    def translate(self, statement):
         sent = sentence(statement)
         state = 0
         self.question = sent.is_question()
         self.error = ""
         phrase = sent.next_phrase()

         while (phrase["error"] == None and phrase["type"] != "eol"):
             self.negative += phrase["negative"]

             if (state == 0):
                 if (phrase["type"] == "unknown" or
                     phrase["type"] == "noun phrase"):
                     self.subj_adj = phrase["adj"]
                     self.subj = phrase["primary"]
                     state = 1 
                 elif (phrase["type"] == "verb"):
                     self.orig_verb = phrase["primary"]
                     if (phrase["primary"] in self.to_be):
                         self.verb = "IS"
                     else:
                         self.verb = phrase["primary"]
                     state = 3
                 elif (phrase["type"] == "helping verb"):
                     self.helping_verb = phrase["helping_verb"]
                     state = 5
                 else:
                     self.error = "I DON'T UNDERSTAND"
                     state = 99

             elif (state == 1):
                 self.orig_verb = phrase["primary"]
                 
                 if (phrase["type"] == "verb"):
                     # Treat forms of "to be" specially
                     if (phrase["primary"] in self.to_be):
                         self.verb = "IS"
                     else:
                         self.verb = phrase["primary"]
                     state = 2
                 elif (phrase["type"] == "verb phrase"):
                     # Treat forms of "to be" specially
                     if (phrase["primary"] in self.to_be):
                         self.helping_verb = phrase["helping_verb"]
                         self.verb = "IS"
                     else:
                         self.helping_verb = phrase["helping_verb"]
                         self.verb = phrase["primary"]
                     state = 2
                 elif (phrase["type"] == "unknown"):
                     self.verb = phrase["primary"]
                     state = 2
                 else:
                     self.error = "I DON'T UNDERSTAND"
                     state = 99

             elif (state == 2):
                 if (phrase["type"] == "unknown"):
                     self.obj = phrase["primary"]
                     state = 99
                 elif (phrase["type"] == "noun phrase"):
                     self.obj_adj = phrase["adj"]
                     self.obj = phrase["primary"]
                     state = 99
                 else:
                     self.error = "I DON'T UNDERSTAND"
                     state = 99

             elif (state == 3):
                 if (phrase["type"] == "unknown"):
                     self.subj = phrase["primary"]
                     state = 4
                 elif (phrase["type"] == "noun phrase"):
                     self.subj_adj = phrase["adj"]
                     self.subj = phrase["primary"]
                     state = 4
                 else:
                     self.error = "I DON'T UNDERSTAND"
                     state = 99

             elif (state == 4):
                 if (phrase["type"] == "unknown"):
                     self.obj = phrase["primary"]
                     state = 99
                 elif (phrase["type"] == "noun phrase"):
                     self.obj_adj = phrase["adj"]
                     self.obj = phrase["primary"]
                     state = 99
                 else:
                     self.error = "I DON'T UNDERSTAND"
                     state = 99

             elif (state == 5):
                 if (phrase["type"] == "unknown"):
                     self.subj = phrase["primary"]
                     state = 6
                 elif (phrase["type"] == "noun phrase"):
                     self.subj_adj = phrase["adj"]
                     self.subj = phrase["primary"]
                     state = 6
                 else:
                     self.error = "I DON'T UNDERSTAND"
                     state = 99
                 
             elif (state == 6):
                 if (phrase["type"] == "unknown"):
                     self.orig_verb = phrase["primary"]
                     if (phrase["primary"] in self.to_be):
                         self.verb = "IS"
                     else:
                         self.verb = phrase["primary"]
                     state = 7
                 else:
                     self.error = "I DON'T UNDERSTAND"
                     state = 99

             elif (state == 7):
                 if (phrase["type"] == "eol"):
                     state = 99
                 elif (phrase["type"] == "unknown"):
                     self.obj = phrase["primary"]
                     state = 99
                 elif (phrase["type"] == "noun phrase"):
                     self.obj_adj = phrase["adj"]
                     self.obj = phrase["primary"]
                     state = 99
                 else:
                     self.error = "I DON'T UNDERSTAND"
                     state = 99

             elif (state == 99):
                 self.error = "THE SENTENCE IS TOO LONG"

             phrase = sent.next_phrase()
             

         if (self.error == ""):
             if (self.negative > 1):
                 self.error = "DON'T GIVE ME NO DOUBLE NEGATIVES"

    ##
    ## Given a fact, return another with its first and second person
    ## pronouns and verbs swapped, so that, for example, "I AM"
    ## becomes "YOU ARE" and vice-versa.
    ##
    ## Inputs:
    ##   none
    ##
    ## Returns:
    ##   A new fact with the first and second person nouns/verbs switched
    ##
    def swap_person(self):
        newfact = copy.copy(self)

        if (self.subj_adj == "MY"):
            newfact.subj_adj = "YOUR"

        if (self.subj_adj == "YOUR"):
            newfact.subj_adj = "MY"

        if (self.subj == "I"):
            if (self.orig_verb == "AM"):
                newfact.subj = "YOU"
                newfact.orig_verb = "ARE"
            elif (self.orig_verb == "WAS"):
                newfact.subj = "YOU"
                newfact.orig_verb = "WERE"
            else:
                newfact.subj = "YOU"
                
        if (self.subj == "YOU"):
            if (self.orig_verb == "ARE"):
                newfact.subj = "I"
                newfact.orig_verb = "AM"
            elif (self.orig_verb == "WERE"):
                newfact.subj = "I"
                newfact.orig_verb = "WAS"
            else:
                newfact.subj = "I"

        if (self.obj_adj == "MY"):
            newfact.obj_adj = "YOUR"

        if (self.obj_adj == "YOUR"):
            newfact.obj_adj = "MY"

        if (self.obj == "ME"):
            newfact.obj = "YOU"

        if (self.obj == "MYSELF"):
            newfact.obj = "YOURSELF"

        if (self.obj == "YOU"):
            newfact.obj = "ME"

        if (self.obj == "YOURSELF"):
            newfact.obj = "MYSELF"

        return newfact
        
    
######################################################################
##
## The brain is ... well ... the brains of the operation.  It's
## where the various facts are stored and where the logical inferences
## between them are made.
##
######################################################################
class brain:
    to_be = [ "IS", "ARE", "BE", "WAS", "WERE" ]
    last_reason = []

    ##
    ## Initialize a new brain object
    ##
    ## Inputs:
    ##   none
    ##
    ## Returns:
    ##   a brain object
    ##
    def __init__(self):
        self.brain = {}
        self.brain_verb = {}
        self.brain_obj = {}

    ##
    ## Convert a brain object into a string.  This is done by
    ## outputting a list of all the facts contained within the
    ## brain.
    ## 
    ## Inputs:
    ##   none
    ##
    ## Returns:
    ##   A string containing all the facts, separated by newlines
    ##
    def __str__(self):
        out = ""
        for s in self.brain.keys():
            for v in self.brain[s].keys():
                for o in self.brain[s][v].keys():
                    if (s != o or v != "IS"):
                        out = out + self.brain[s][v][o].__str__() + "\n"

        return out

    ##
    ## Load a session from a file
    ##
    ## Inputs:
    ##   filename: The file from which to read
    ##
    ## Returns:
    ##   A list containing any rejects
    ##
    ## Exceptions:
    ##   DeduceError, IOError
    ##
    def load(self, filename=""):
        if (filename == ""):
            raise DeduceError, "LOAD requires a filename"
        else:
            f = file(filename, "rU")
            rejects = []
            count = 0

            for line in iter(f):
                count += 1
                line = line.strip()

                if (len(line) > 0):
                    response = self.learn(line)
                    status = response["STATUS"]

                    if (status == -1):
                        msg = response["MSG"]
                        rejects.append("line "+count.__str__()+": "+line+" ("+msg+")")
                        
                if (count >= 10000):
                    raise DeduceError, "Too many input lines, bailing out"
                    break
                        
            f.close()
            return rejects

    ##
    ## Save a session to a file
    ##
    ## Inputs:
    ##   filename: The file to which to write
    ##
    ## Returns:
    ##   none
    ##
    ## Exceptions:
    ##   DeduceError, IOError
    ##
    def save(self, filename=""):
        if (filename == ""):
            raise DeduceError, "SAVE requires a filename"
        else:
            f = file(filename, "w")
            f.write(self.__str__())
            f.close()

    ##
    ## Return a list of all the facts in the brain
    ##
    ## Inputs:
    ##   none
    ##
    ## Returns:
    ##   A list of facts
    ##
    def divulge(self):
        stuff = []

        for subj in self.brain.keys():
            for verb in self.brain[subj].keys():
                for obj, fact in self.brain[subj][verb].items():
                    stuff.append(fact)

        return stuff

    ##
    ## Ask the question "why?"
    ##
    ## Inputs:
    ##   question: A question to explain.  If blank, the last question
    ##     or statement is used
    ##
    ## Returns:
    ##   A list containing facts to explain the answer
    ##
    def why(self, question=""):
        if (question == ""):
            # No question, return the last formed reason (usually from the
            # last question or rejected statement)
            return self.last_reason
        else:
            # Ask the question and collect the reason
            [ stat, str, reason ] = self.query(question)
            return reason

    ##
    ## Learn is used to teach something to the brain.  If the information
    ## can already be deduced right or wrong, the new information will
    ## be rejected.
    ##
    ## Inputs:
    ##   newfact: A fact or string containing the information to add
    ##
    ## Returns:
    ##   A dictionary with the following entries:
    ##     STATUS: 0 if the fact was learned, -1 if not.
    ##     MSG:    A string with an English response.
    ##     REASON: A list containing the reason for the reply
    ##
    def learn(self, newfact):
        # Translate the fact from a string to a true fact if necessary
        if (type(newfact) == type("")):
            newfact = fact(newfact)

        # Make sure the fact has no errors
        if (newfact.error != ""):
            return { 'STATUS': -1,
                     'MSG': newfact.error,
                     'REASON': [] }

        # Verify that we have a real sentence
        if (newfact.subj != "" and newfact.verb == ""):
            return { 'STATUS': -1,
                     'MSG': "I DON'T UNDERSTAND",
                     'REASON': [] }

        # Refuse anything that says something is not itself
        if (newfact.subj == newfact.obj and newfact.negative == 1):
            return { 'STATUS': -1,
                     'MSG': "I'M NOT BUYING IT",
                     'REASON': [] }

        # Query to make sure this is a new fact
        [ stat, str, reason ] = self.query(newfact)
        if (stat == 0):
            return { 'STATUS': -1,
                     'MSG': "THAT CAN'T BE RIGHT",
                     'REASON': reason }
        elif (stat == 1):
            return { 'STATUS': -1,
                     'MSG': "YEAH, I KNOW",
                     'REASON': reason }
            
        # Insert the subject
        if (not self.brain.has_key(newfact.subj)):
            self.brain[newfact.subj] = {}

        # Insert the verb
        if (not self.brain[newfact.subj].has_key(newfact.verb)):
            self.brain[newfact.subj][newfact.verb] = {}

        # If there's no object, assign a default one
        if (newfact.obj == ""):
            newfact.obj = ".fact"

        # Insert the object
        if (self.brain[newfact.subj][newfact.verb].has_key(newfact.obj)):
            oldfact = self.brain[newfact.subj][newfact.verb][newfact.obj]
            if (newfact.negative == oldfact.negative):
                return { 'STATUS': -1,
                         'MSG': "YEAH, I KNOW",
                         'REASON': [oldfact] }
            else:
                return { 'STATUS': -1,
                         'MSG': "THAT CAN'T BE RIGHT ("+oldfact.__str__()+")",
                         'REASON': [oldfact] }
        else:
            self.brain[newfact.subj][newfact.verb][newfact.obj] = newfact

        # Store the verb and object for future reference
        if (not self.brain_verb.has_key(newfact.verb)):
            self.brain_verb[newfact.verb] = 1

        # Also store the object
        if (newfact.obj != "" and not self.brain_obj.has_key(newfact.obj)):
            self.brain_obj[newfact.obj] = 1

        if (newfact.subj == newfact.obj):
            return { 'STATUS': 0,
                     'MSG': "WELL, OK",
                     'REASON': [] }
        else:
            return { 'STATUS': 0,
                     'MSG': "OK",
                     'REASON': [] }

    ##
    ## Return a list of facts relating to a particular subject.  This
    ## list will include items determined deductively.
    ##
    ## Inputs:
    ##   subject: The subject we want to describe
    ##
    ## Returns:
    ##   A list of facts about the given subject
    ##
    def describe_subj(self, subject):
        subject = upper(subject)
        dfact = fact("")
        symmetry = fact(subject+" is "+subject)
        desc = [ symmetry ]

        if (not self.brain.has_key(subject)):
            return desc

        for verb in self.brain[subject]:
            for object in self.brain[subject][verb]:
                dfact = copy.deepcopy(self.brain[subject][verb][object])

                if (dfact.seen == 0):
                    desc.append(dfact)

                    if (not dfact.verb in self.to_be):
                        if (dfact.obj != ".fact" and not dfact.negative):
                            #dfact.obj = ""
                            #dfact.obj_adj = "" # commented out for description display
                            desc.append(dfact)
                    elif (not dfact.negative):
                        if (object == ".fact"):
                            newsubj = dfact.verb
                        else:
                            newsubj = dfact.obj

                        # Now, recursively look for more entries
                        self.brain[subject][verb][object].seen = 1
                        desc2 = self.describe_subj(newsubj)
                        self.brain[subject][verb][object].seen = 0

                        # Convert the subject of each fact
                        for index in xrange(0,len(desc2)):
                            desc2[index].subj_adj = dfact.subj_adj
                            desc2[index].subj = dfact.subj

                        desc += desc2
                        del desc2

        return desc

    ##
    ## Ask a yes/no question to a brain.  The brain will recursively
    ## search for an answer based on the facts it knows
    ##
    ## Inputs:
    ##   question: The question to ask
    ##
    ## Returns:
    ##   A number detailing the answerm, a string containing the answer, and
    ##   a list of facts detailing the reason for the answer (assuming the
    ##   answer isn't "I don't know")
    ##
    def query(self, question):
        # Translate the question from a string to a fact if necessary
        if (type(question) == type("")):
            question = fact(question)

        # Simple checks
        if (not self.brain.has_key(question.subj)
            and not self.brain_obj.has_key(question.subj)):
            
            return [ -1, "WHAT'S THIS \""+question.subj+"\" THING?", [] ]

        if (question.obj != ""
            and not self.brain.has_key(question.obj)
            and not self.brain_obj.has_key(question.obj)):
            
            return [ -1, "WHAT'S THIS \""+question.obj+"\" THING?", [] ]

        if (question.verb != "IS"):
            if (not self.brain_verb.has_key(question.verb)):
                return [ -1, "I DON'T KNOW WHAT IT MEANS TO "+question.verb, [] ]

        if (question.subj == question.obj and question.verb == "IS"):
            if (question.negative == 0):
                self.last_reason = [ question ]
                return [ 1, "DUH, YES", [question] ]
            else:
                question.negative = 1 - question.negative
                self.last_reason = [ question ]
                return [ 0, "UGH, NO", [question] ]
        
        # Ask the question
        [ans,reason] = self.query_i(question)
        self.last_reason = reason

        if (ans == 0):
            question.negative = 1 - question.negative
            return [ 0, "NO, "+question.swap_person().__str__(), reason ]
        elif (ans == 1):
            return [ 1, "YES, "+question.swap_person().__str__(), reason ]

        # More checking is necessary here, just to be sure.
        # Get a list of every attribute of both the subject and object.
        if (question.obj != "" and question.verb == "IS"):
            subject_attr = self.describe_subj(question.subj)
            object_attr = self.describe_subj(question.obj)

            for f in subject_attr:
                f2 = copy.copy(f)
                f2.subj = question.obj
                f2.subj_adj = question.obj_adj
                [ans,reason] = self.query_i(f2)
                if (ans == 0):
                    # Grab the reason for the fact f
                    [ans,reason0] = self.query_i(f)
                    reason = reason0 + reason
                    self.last_reason = reason

                    if (question.negative == 0):
                        question.negative = 1 - question.negative
                        return [ 0, "NO, "+question.swap_person().__str__(), reason ]
                    else :
                        return [ 1, "YES, "+question.swap_person().__str__(), reason ]

            for f in object_attr:
                f2 = copy.copy(f)
                f2.subj = question.subj
                f2.subj_adj = question.subj_adj
                [ans,reason] = self.query_i(f2)
                if (ans == 0):
                    # Grab the reason for the fact f
                    [ans,reason0] = self.query_i(f)
                    reason = reason0 + reason
                    self.last_reason = reason
                    
                    if (question.negative == 0):
                        question.negative = 1 - question.negative
                        return [ 0, "NO, "+question.swap_person().__str__(), reason ]
                    else :
                        return [ 1, "YES, "+question.swap_person().__str__(), reason ]

        return [ -1, "I DON'T KNOW", [] ]
    
    ##
    ## Internal function for asking a question.  This is where the real
    ## processing takes place.
    ##
    ## Inputs:
    ##   q: The question to ask (must be a class fact)
    ##
    ## Returns:
    ##   A list containing two values.  The first is:
    ##     -1: I don't know
    ##      0: No
    ##      1: Yes
    ##   and the second is a list of facts detailing the the reason for
    ##   the decision.  If the answer is -1, the list will be empty.
    ##
    def query_i(self, q):
        # Look for the question's subject in the brain
        if (not self.brain.has_key(q.subj)):
            return [ -1, [] ]

        # If the question's verb is of the form "to be" we handle it like this
        if (q.verb == "IS"):
            if (q.subj == q.obj):
                # We can safely say an object is itself
                return [ 1, [q] ]
            if (self.brain[q.subj].has_key(q.verb)):
                # Found a match in the brain.  We can answer definitively
                # yes(1) or no(0)
                if (self.brain[q.subj][q.verb].has_key(q.obj)):
                    ans = self.brain[q.subj][q.verb][q.obj].negative

                    if (q.negative == 0):
                        ans = 1 - ans

                    return [ ans,[self.brain[q.subj][q.verb][q.obj]] ]
                else:
                    # Didn't find a definitive answer.  Look for it
                    # recursively
                    for obj in self.brain[q.subj][q.verb]:
                        if (self.brain[q.subj][q.verb][obj].negative == 0):
                            if (self.brain[q.subj]["IS"][obj].seen == 0):
                                q2 = copy.copy(q)
                                q2.subj_adj = self.brain[q.subj][q.verb][obj].obj_adj
                                q2.subj = obj
                                self.brain[q.subj][q.verb][obj].seen = 1
                                [ ans,reason ] = self.query_i(q2)
                                self.brain[q.subj][q.verb][obj].seen = 0
                                if (ans == 0 or ans == 1):
                                    reason = [self.brain[q.subj][q.verb][obj]] + reason
                                    return [ ans,reason ]

            # If we reach this point, the query didn't find anything
            return [ -1,[] ]

        # If the question has a verb but not object, we do this
        elif (q.obj == "" or q.obj == ".fact"):
            if (self.brain[q.subj].has_key(q.verb)):
                if (self.brain[q.subj][q.verb].has_key(".fact")):
                    # See if we can find an exact match to the question
                    ans = self.brain[q.subj][q.verb][".fact"].negative
                    if (q.negative == 0):
                        ans = 1 - ans
                    return [ ans,[self.brain[q.subj][q.verb][".fact"]] ]

                # See if we can find a matching, non-negative match with
                # a different object
                for obj in self.brain[q.subj][q.verb]:
                    if self.brain[q.subj][q.verb][obj].negative == 0:
                        return [ 1,[self.brain[q.subj][q.verb][obj]] ]

                # No match, then the answer is unknown
                return [ -1,[] ]
            else:
                # Replace the subject and search recursively
                if self.brain[q.subj].has_key("IS"):
                    for obj in self.brain[q.subj]["IS"]:
                        if (self.brain[q.subj]["IS"][obj].seen == 0
                            and self.brain[q.subj]["IS"][obj].negative == 0):
                            q2 = copy.copy(q)
                            q2.subj = obj
                            q2.subj_adj = self.brain[q.subj]["IS"][obj].obj_adj
                            self.brain[q.subj]["IS"][obj].seen = 1
                            [ans,reason] = self.query_i(q2)
                            self.brain[q.subj]["IS"][obj].seen = 0

                            if (ans == 0 or ans == 1):
                                reason = [self.brain[q.subj]["IS"][obj]] + reason
                                return [ ans,reason ]
                    
                return [ -1,[] ]
        else:
            # The question has a subject, verb, and object
            if (self.brain[q.subj].has_key(q.verb)):
                if (self.brain[q.subj][q.verb].has_key(q.obj)):
                    # Found an exact match
                    ans = self.brain[q.subj][q.verb][q.obj].negative
                    if (q.negative == 0):
                        ans = 1 - ans
                    return [ ans,[self.brain[q.subj][q.verb][q.obj]] ]

            # Recursively replace the subject
            if (self.brain[q.subj].has_key("IS")):
                for obj in self.brain[q.subj]["IS"]:
                    if (self.brain[q.subj]["IS"][obj].seen == 0
                        and self.brain[q.subj]["IS"][obj].negative == 0):
                        q2 = copy.copy(q)
                        q2.subj_adj = self.brain[q.subj]["IS"][obj].obj_adj
                        q2.subj = obj
                        self.brain[q.subj]["IS"][obj].seen = 1
                        [ans,reason] = self.query_i(q2)
                        self.brain[q.subj]["IS"][obj].seen = 0

                        if (ans == 0 or ans == 1):
                            reason = [self.brain[q.subj]["IS"][obj]] + reason
                            return [ ans,reason ]

            # Try to find another word to use in place of the object
            if (self.brain.has_key(q.obj)):
                if (self.brain[q.obj].has_key("IS")):
                    for obj in self.brain[q.obj]["IS"]:
                        if (self.brain[q.obj]["IS"][obj].negative == 0):
                            q2 = copy.copy(q)
                            q2.obj_adj = self.brain[q.obj]["IS"][obj].obj_adj
                            q2.obj = obj
                            self.brain[q.obj]["IS"][obj].seen = 1
                            [ans,reason] = self.query_i(q2)
                            self.brain[q.obj]["IS"][obj].seen = 0
                            if (ans == 1):
                                reason = [self.brain[q.obj]["IS"][obj]] + reason
                                return [ ans,reason ]
                
            return [ -1,[] ]

### MAIN ###
if __name__ == "__main__":
    import deduce_console
    deduce_console.ui()

